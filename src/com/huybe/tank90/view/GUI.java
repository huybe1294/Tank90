package com.huybe.tank90.view;

import com.huybe.tank90.manager.ImageStore;
import com.huybe.tank90.model.MapItem;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;

public class GUI extends JFrame implements Setup {
    public static final int WIDTH_FRAME = 28 * MapItem.SIZE + 66;
    public static final int HEIGHT_FRAME = 28 * MapItem.SIZE + 28;

    private GamePanel gamePanel;

    public GUI() {
        initComponents();
        addComponents();
        registerListener();
    }

    @Override
    public void initComponents() {
        setTitle("Tank90");
        setLayout(new CardLayout());
        setSize(WIDTH_FRAME, HEIGHT_FRAME);
        setResizable(false);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
    }

    @Override
    public void addComponents() {
        gamePanel = new GamePanel();
        add(gamePanel);

        setIconImage(ImageStore.IMG_ICON);
    }

    @Override
    public void registerListener() {
    }
}

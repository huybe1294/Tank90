package com.huybe.tank90.view;

public interface Setup {
    void initComponents();

    void addComponents();

    void registerListener();
}


package com.huybe.tank90.view;


import javax.swing.*;

public abstract class BasePanel extends JPanel implements Setup {

    public BasePanel() {
        initComponents();
        addComponents();
        registerListener();
    }
}

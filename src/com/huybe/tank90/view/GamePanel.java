package com.huybe.tank90.view;

import com.huybe.tank90.manager.GameManager;
import com.huybe.tank90.manager.ImageStore;
import com.huybe.tank90.manager.SoundManager;
import com.huybe.tank90.model.Tank;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class GamePanel extends BasePanel implements Runnable {
    private Thread thread;
    private GameManager gameManager;
    private boolean flagUp;
    private boolean flagDown;
    private boolean flagLeft;
    private boolean flagRight;
    private SoundManager soundManager;
    private JLabel jLabelRestart;

    @Override
    public void initComponents() {
        setLayout(null);
        setBackground(Color.BLACK);
        setFocusable(true);
    }

    @Override
    public void addComponents() {
        gameManager = new GameManager();
    }

    @Override
    public void registerListener() {
        KeyAdapter keyAdapter = new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent keyEvent) {
                doListenerKey(keyEvent, true);
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
                doListenerKey(keyEvent, false);
            }
        };
        addKeyListener(keyAdapter);
    }

    private void doListenerKey(KeyEvent keyEvent, boolean value) {
        int keyCode = keyEvent.getKeyCode();
        switch (keyCode) {
            case KeyEvent.VK_UP:
                flagUp = value;
                break;

            case KeyEvent.VK_DOWN:
                flagDown = value;
                break;

            case KeyEvent.VK_LEFT:
                flagLeft = value;
                break;

            case KeyEvent.VK_RIGHT:
                flagRight = value;
                break;

            case KeyEvent.VK_SPACE:
                gameManager.fire();
                if (gameManager.getGameState() == GameManager.GAME_PLAY) {
                    soundManager.shoot();
                }
                break;

            case KeyEvent.VK_S:
                if (gameManager.getGameState() == GameManager.GAME_START) {
                    gameManager.setGameState(GameManager.GAME_PLAY);
                    startGame();
                    soundManager.enterGame();
                }
                break;

            default:
                break;
        }
    }

    public void startGame() {
        thread = new Thread(this);
        soundManager = new SoundManager();
        thread.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D graphics2D = (Graphics2D) g;
        graphics2D.drawImage(ImageStore.IMG_BAIT, 0, 0, this); //bait
        gameManager.draw(graphics2D);
    }

    @Override
    public void run() {
        while (gameManager.getGameState() == GameManager.GAME_PLAY) {
            System.out.println(gameManager.getGameState());
            if (flagUp) {
                gameManager.moveTank(Tank.UP);
            }
            if (flagDown) {
                gameManager.moveTank(Tank.DOWN);
            }
            if (flagLeft) {
                gameManager.moveTank(Tank.LEFT);
            }
            if (flagRight) {
                gameManager.moveTank(Tank.RIGHT);
            }

            gameManager.update();
            if (gameManager.handleWinGame()) {
                thread.stop();
            }
            handleGameRestart();
            try {
                thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            repaint();
        }
    }

    private void handleGameRestart() {
        if (gameManager.getGameState() == GameManager.GAME_OVER) {
            jLabelRestart = new JLabel();
            jLabelRestart.setBounds(250, 250, 114, 42);
            jLabelRestart.setIcon(ImageStore.IMG_RESTART_NORMAL);
            add(jLabelRestart);
            jLabelRestart.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseEntered(MouseEvent e) {
                    jLabelRestart.setIcon(ImageStore.IMG_RESTART_ACTIVE);
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    jLabelRestart.setIcon(ImageStore.IMG_RESTART_NORMAL);
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    gameManager.setGameState(GameManager.GAME_START);
                    gameManager = new GameManager();
                    jLabelRestart.setVisible(false);
                }
            });
        }
    }
}
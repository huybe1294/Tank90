package com.huybe.tank90.manager;

import javax.swing.*;
import java.awt.*;

public class ImageStore {
    public static final Image IMG_BOSS = getImage("img_boss.png");
    public static final Image IMG_BRICK = getImage("img_brick.png");
    public static final Image IMG_ROCK = getImage("img_rock.png");
    public static final Image IMG_TREE = getImage("img_tree.png");
    public static final Image IMG_WATER = getImage("img_water.png");
    public static final Image IMG_PLAYER_LEFT = getImage("player_left.png");
    public static final Image IMG_PLAYER_RIGHT = getImage("player_right.png");
    public static final Image IMG_PLAYER_UP = getImage("player_up.png");
    public static final Image IMG_PLAYER_DOWN = getImage("player_down.png");
    public static final Image IMG_ENEMY_LEFT = getImage("enemy_left.png");
    public static final Image IMG_ENEMY_RIGHT = getImage("enemy_right.png");
    public static final Image IMG_ENEMY_UP = getImage("enemy_up.png");
    public static final Image IMG_ENEMY_DOWN = getImage("enemy_down.png");
    public static final Image IMG_BULLET = getImage("bullet.png");
    public static final Image IMG_HEART = getImage("img_heart.png");
    public static final Image IMG_ICON = getImage("icon.png");
    public static final Image IMG_BAIT = getImage("bird_blue.gif");

    public static final Icon IMG_RESTART_NORMAL = new ImageIcon(ImageStore.class.getResource("/res/drawable/img_restart_normal.png"));
    public static final Icon IMG_RESTART_ACTIVE = new ImageIcon(ImageStore.class.getResource("/res/drawable/img_restart_active.png"));


    private static Image getImage(String nameImage) {
        String path = ImageStore.class.getResource("/res/drawable/" + nameImage).getPath();
        return new ImageIcon(path).getImage();
    }
}

package com.huybe.tank90.manager;

import com.huybe.tank90.model.*;
import com.huybe.tank90.view.GUI;

import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameManager {
    public static final int GAME_START = 0;
    public static final int GAME_PLAY = 1;
    public static final int GAME_OVER = 2;

    private int gameState = GAME_START;

    private List<MapItem> waters;
    private List<MapItem> trees;
    private List<MapItem> boss;
    private List<MapItem> rocks;
    private List<MapItem> bricks;

    private List<Tank> tanks;
    private List<Tank> enemyTanks;

    private List<Bullet> bulletTanks;
    private List<Bullet> bulletEnemyTanks;
    private List<Heart> hearts;

    private Random random;
    private int countEnemyTank = 0;
    private int countTank = 0;
    private SoundManager soundManager;


    public GameManager() {
        random = new Random();

        waters = new ArrayList<>();
        trees = new ArrayList<>();
        boss = new ArrayList<>();
        rocks = new ArrayList<>();
        bricks = new ArrayList<>();

        tanks = new ArrayList<>();
        enemyTanks = new ArrayList<>();
        bulletTanks = new ArrayList<>();
        bulletEnemyTanks = new ArrayList<>();
        hearts = new ArrayList<>();

        soundManager = new SoundManager();
        initData();
        System.out.println(gameState);
    }

    public void initData() {
        initTank();
        initializeMap();
        initHeart();
        countTank = 0;
        countEnemyTank = 0;
    }

    public void draw(Graphics2D graphics2D) {
        drawTank(graphics2D);
        drawEnemyTank(graphics2D);
        drawMapItems(graphics2D);
        drawHeart(graphics2D);
        drawBullet(graphics2D);

        Font font = new Font("Cooper Black", Font.BOLD, 46);
        graphics2D.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        graphics2D.setFont(font);
        graphics2D.setColor(Color.WHITE);

        String strStart = "Press S to start";
        int widthStrStart = graphics2D.getFontMetrics(font).stringWidth(strStart);
        int heightStrStart = graphics2D.getFontMetrics(font).getHeight();
        String strGameOver = "Game Over";

        int widthStrRestart = graphics2D.getFontMetrics(font).stringWidth(strGameOver);
        int heightStrRestart = graphics2D.getFontMetrics(font).getHeight();

        if (gameState == GAME_START) {
            graphics2D.drawString(strStart, (GUI.WIDTH_FRAME - widthStrStart) / 2, (GUI.HEIGHT_FRAME) / 2 - heightStrStart);
        } else if (gameState == GAME_OVER) {
            graphics2D.drawString(strGameOver, (GUI.WIDTH_FRAME - widthStrRestart) / 2, (GUI.HEIGHT_FRAME) / 2 - heightStrRestart);
        }

        //handle Wingame
        String strEndgame = "You Are Winner";
        int widthStrEndgame = graphics2D.getFontMetrics(font).stringWidth(strEndgame);
        int heightStrEndgame = graphics2D.getFontMetrics(font).getHeight();
        if (countEnemyTank == 50) {
            graphics2D.drawString(strEndgame, (GUI.WIDTH_FRAME - widthStrEndgame) / 2, (GUI.HEIGHT_FRAME) / 2 - heightStrEndgame);
        }

        graphics2D.setFont(new Font("Arial", Font.BOLD, 16));
        graphics2D.drawString("P:" + countEnemyTank, 560, 100);

    }

    public void update() {
        moveEnemytank();
        moveBullet();
        fireEnemyTank();
        handleCollisionBullet();
        remove();
        initNewTank();
        if (handleEndGame()) {
            setGameState(GAME_OVER);
        }
    }

    private void initializeMap() {
        try {
            String path = getClass().getResource("/res/assets/map1.txt").getPath();
            File file = new File(path);

            BufferedReader input = new BufferedReader(new FileReader(file));

            String line = input.readLine();
            int i = 0;
            while (line != null) {
                for (int j = 0; j < line.length(); j++) {
                    int type = line.charAt(j) - 48;
                    MapItem item = new MapItem(
                            j * MapItem.SIZE,
                            i * MapItem.SIZE,
                            type);
                    switch (type) {
                        case MapItem.WATER:
                            waters.add(item);
                            break;

                        case MapItem.TREE:
                            trees.add(item);
                            break;

                        case MapItem.BOSS:
                            boss.add(item);
                            break;

                        case MapItem.BRICK:
                            bricks.add(item);
                            break;

                        case MapItem.ROCK:
                            rocks.add(item);
                            break;

                        default:
                            break;
                    }
                }
                i++;
                line = input.readLine();
            }

            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initTank() {
        tanks.clear();
        enemyTanks.clear();
        tanks.add(new Tank(200, 515, Tank.UP));

        enemyTanks.add(new Tank(25, 25, Tank.RIGHT));
        enemyTanks.add(new Tank(270, 25, Tank.DOWN));
        enemyTanks.add(new Tank(515, 25, Tank.LEFT));
    }

    public void initNewTank() {

        if (enemyTanks.size() < 5) {
            random = new Random();
            int pos = random.nextInt(3);
            int x = 0;
            int y = Tank.yEnemy;
            switch (pos) {
                case 1:
                    x = Tank.x1Enemy;
                    break;
                case 2:
                    x = Tank.x2Enemy;
                    break;
                case 3:
                    x = Tank.x3Enemy;
                    break;
                default:
                    break;
            }
            Tank tank = new Tank(x, y, Tank.DOWN);
            boolean isExist = tank.isCollisionWithTank(enemyTanks);
            if (!isExist) {
                enemyTanks.add(tank);
            }
        }
        if (tanks.size() == 0) {
            tanks.add(new Tank(200, 515, Tank.UP));
        }
    }

    public void initHeart() {
        hearts.clear();
        hearts.add(new Heart(590, 50));
        hearts.add(new Heart(575, 50));
        hearts.add(new Heart(560, 50));
    }

    public void drawMapItems(Graphics2D graphics2D) {
        for (int i = 0; i < waters.size(); i++) {
            waters.get(i).draw(graphics2D);
        }
        for (int i = 0; i < trees.size(); i++) {
            trees.get(i).draw(graphics2D);
        }
        for (int i = 0; i < boss.size(); i++) {
            boss.get(i).draw(graphics2D);
        }
        for (int i = 0; i < rocks.size(); i++) {
            rocks.get(i).draw(graphics2D);
        }
        for (int i = 0; i < bricks.size(); i++) {
            bricks.get(i).draw(graphics2D);
        }
    }

    public void drawHeart(Graphics2D graphics2D) {
        for (int i = 0; i < hearts.size(); i++) {
            hearts.get(i).draw(graphics2D);
        }
    }

    public void drawTank(Graphics2D graphics2D) {
        for (int i = 0; i < tanks.size(); i++) {
            tanks.get(i).draw(graphics2D);
        }
    }

    public void drawEnemyTank(Graphics2D graphics2D) {
        for (int i = 0; i < enemyTanks.size(); i++) {
            enemyTanks.get(i).drawEnemyTank(graphics2D);
        }
    }

    public void moveTank(int newOrient) {
        for (int i = 0; i < tanks.size(); i++) {
            tanks.get(i).setOrient(newOrient);
            tanks.get(i).move(getAllMapItems(), enemyTanks);
        }
    }

    public void moveEnemytank() {
        for (int i = 0; i < enemyTanks.size(); i++) {
            enemyTanks.get(i).moveEnemyTank(getAllMapItems(), tanks);
        }
    }

    public void fire() {
        for (int i = 0; i < tanks.size(); i++) {
            tanks.get(i).fire(bulletTanks);
        }
    }

    public void fireEnemyTank() {
        for (int i = 0; i < enemyTanks.size(); i++) {
            enemyTanks.get(i).fireEnemyTank(bulletEnemyTanks);
        }
    }

    public void drawBullet(Graphics2D graphics2D) {
        for (int i = 0; i < bulletTanks.size(); i++) {
            bulletTanks.get(i).draw(graphics2D);
        }
        for (int i = 0; i < bulletEnemyTanks.size(); i++) {
            bulletEnemyTanks.get(i).draw(graphics2D);
        }

    }

    public void moveBullet() {
        for (int i = 0; i < bulletTanks.size(); i++) {
            bulletTanks.get(i).move();
        }
        for (int i = 0; i < bulletEnemyTanks.size(); i++) {
            bulletEnemyTanks.get(i).move();
        }
    }

    public void remove() {
        //remove bullet
        List<Bullet> list1 = new ArrayList<>();
        List<Bullet> list2 = new ArrayList<>();
        list1.addAll(getBulletTanks());
        list2.addAll(getBulletEnemyTanksTanks());
        for (int i = 0; i < list1.size(); i++) {
            if (list1.get(i).isLive() == false) {
                list1.remove(i);
                i--;
            }
            setBulletTanks(list1);
        }
        for (int i = 0; i < list2.size(); i++) {
            if (list2.get(i).isLive() == false) {
                list2.remove(i);
                i--;
            }
            setBulletEnemyTanks(list2);
        }
    }

    public void handleCollisionBullet() {
        //Bullet vs MapItem
        for (int i = 0; i < bulletTanks.size(); i++) {
            if (bulletTanks.get(i).isCollisionWithMap(getAllMapItems())) {
                bulletTanks.get(i).setLive(false);
            }
        }
        for (int i = 0; i < bulletEnemyTanks.size(); i++) {
            if (bulletEnemyTanks.get(i).isCollisionWithMap(getAllMapItems())) {
                bulletEnemyTanks.get(i).setLive(false);
            }
        }

        //Bullet vs Brick. Remove brick
        for (int i = 0; i < bricks.size(); i++) {
            if (bricks.get(i).isCollisionWithBullet(bulletTanks)) {
                bricks.remove(i);
            } else if (bricks.get(i).isCollisionWithBullet(bulletEnemyTanks)) {
                bricks.remove(i);
            }
        }
        //BulletEnemyTank vs Tank
        for (int i = 0; i < bulletEnemyTanks.size(); i++) {
            if (bulletEnemyTanks.get(i).isCollisionWithTank(tanks)) {
                bulletEnemyTanks.get(i).setLive(false);
                soundManager.explosion_tank();
            }
        }
        for (int i = 0; i < tanks.size(); i++) {
            if (tanks.get(i).isCollideWithBullet(bulletEnemyTanks)) {
                tanks.remove(i);
                countTank++;
                hearts.remove(i);
            }
        }
        //BulletTank vs EnemyTank
        for (int i = 0; i < bulletTanks.size(); i++) {
            if (bulletTanks.get(i).isCollisionWithTank(enemyTanks)) {
                bulletTanks.get(i).setLive(false);
                soundManager.explosion_tank();
            }
        }
        for (int i = 0; i < enemyTanks.size(); i++) {
            if (enemyTanks.get(i).isCollideWithBullet(bulletTanks)) {
                enemyTanks.remove(i);
                countEnemyTank++;
            }
        }

        //Bullet vs Bullet
        for (int i = 0; i < bulletTanks.size(); i++) {
            if (bulletTanks.get(i).isCollideWithBullet(bulletEnemyTanks)) {
                bulletTanks.get(i).setLive(false);
            }
        }
        for (int i = 0; i < bulletEnemyTanks.size(); i++) {
            if (bulletEnemyTanks.get(i).isCollideWithBullet(bulletTanks)) {
                bulletEnemyTanks.get(i).setLive(false);
            }
        }
        //Boss vs Bullet
        for (int i = 0; i < boss.size(); i++) {
            if (boss.get(i).isCollisionWithBullet(bulletTanks)) {
                gameState = GAME_OVER;
            }
            if (boss.get(i).isCollisionWithBullet(bulletEnemyTanks)) {
                gameState = GAME_OVER;
            }
        }
    }

    public boolean handleWinGame() {
        if (countEnemyTank == 50) {
            return true;
        }
        return false;
    }

    public boolean handleEndGame() {
        if (countTank == 3) {
            return true;
        }
        return false;
    }

    public List<MapItem> getAllMapItems() {
        List<MapItem> list = new ArrayList<>();
        list.addAll(waters);
        list.addAll(boss);
        list.addAll(rocks);
        list.addAll(bricks);
        return list;
    }

    public List<Bullet> getBulletTanks() {
        return bulletTanks;
    }

    public List<Bullet> getBulletEnemyTanksTanks() {
        return bulletEnemyTanks;
    }

    public void setBulletTanks(List<Bullet> bullets) {
        this.bulletTanks = bullets;
    }

    public void setBulletEnemyTanks(List<Bullet> bullets) {
        this.bulletEnemyTanks = bullets;
    }

    public int getGameState() {
        return gameState;
    }

    public void setGameState(int gameState) {
        this.gameState = gameState;
    }

    public int getCountEnemyTank() {
        return countEnemyTank;
    }
}

package com.huybe.tank90.manager;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import java.io.File;

public class SoundManager {
    public static final String ENTER_GAME = SoundManager.class.getResource("/res/raw/enter_game.wav").getPath();
    public static final String EXPLOSION = SoundManager.class.getResource("/res/raw/explosion.wav").getPath();
    public static final String EXPLOSION_TANK = SoundManager.class.getResource("/res/raw/explosion_tank.wav").getPath();
    public static final String MOVE = SoundManager.class.getResource("/res/raw/move.wav").getPath();
    public static final String SHOOT = SoundManager.class.getResource("/res/raw/shoot.wav").getPath();

    private AudioInputStream audioInputStream;
    private Clip clip;

    public void playSound(File path) {
        try {
            audioInputStream = AudioSystem.getAudioInputStream(path);
            clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void enterGame() {
        playSound(new File(ENTER_GAME));
    }

    public void explosion() {
        playSound(new File(EXPLOSION));
    }

    public void explosion_tank() {
        playSound(new File(EXPLOSION_TANK));
    }

    public void move() {
        playSound(new File(MOVE));
    }

    public void shoot() {
        playSound(new File(SHOOT));
    }

}

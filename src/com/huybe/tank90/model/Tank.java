package com.huybe.tank90.model;


import com.huybe.tank90.manager.ImageStore;
import com.huybe.tank90.manager.SoundManager;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.List;
import java.util.Random;


public class Tank {
    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int LEFT = 2;
    public static final int RIGHT = 3;

    public static final int SIZE = 18;
    public static final int SPEED = 1;

    public static final int x1Enemy = 25;
    public static final int x2Enemy = 270;
    public static final int x3Enemy = 515;
    public static final int yEnemy = 25;

    private int x;
    private int y;
    private int orient;
    private long time;
    private Random random;
    private SoundManager soundManager;

    public Tank(int x, int y, int orient) {
        this.x = x;
        this.y = y;
        this.orient = orient;
        soundManager = new SoundManager();
    }

    public void draw(Graphics2D graphics2D) {
        Image imgTank = null;
        switch (orient) {
            case LEFT:
                imgTank = ImageStore.IMG_PLAYER_LEFT;
                break;
            case RIGHT:
                imgTank = ImageStore.IMG_PLAYER_RIGHT;
                break;
            case UP:
                imgTank = ImageStore.IMG_PLAYER_UP;
                break;
            case DOWN:
                imgTank = ImageStore.IMG_PLAYER_DOWN;
                break;
            default:
                break;
        }
        graphics2D.drawImage(imgTank, x, y, SIZE, SIZE, null);
    }

    public void drawEnemyTank(Graphics2D graphics2D) {
        Image imgTank = null;
        switch (orient) {
            case LEFT:
                imgTank = ImageStore.IMG_ENEMY_LEFT;
                break;
            case RIGHT:
                imgTank = ImageStore.IMG_ENEMY_RIGHT;
                break;
            case UP:
                imgTank = ImageStore.IMG_ENEMY_UP;
                break;
            case DOWN:
                imgTank = ImageStore.IMG_ENEMY_DOWN;
                break;
            default:
                break;
        }
        graphics2D.drawImage(imgTank, x, y, SIZE, SIZE, null);
    }

    public void move(List<MapItem> mapItems, List<Tank> tanks) {
        switch (orient) {
            case LEFT:
                if (!isCollisionTankAndMap(-1, 0, mapItems) && !isCollisionTankAndTank(-1, 0, tanks)) {
                    x -= SPEED;
                }
                break;
            case RIGHT:
                if (!isCollisionTankAndMap(1, 0, mapItems) && !isCollisionTankAndTank(1, 0, tanks)) {
                    x += SPEED;
                }
                break;
            case UP:
                if (!isCollisionTankAndMap(0, -1, mapItems) && !isCollisionTankAndTank(0, -1, tanks)) {
                    y -= SPEED;
                }
                break;
            case DOWN:
                if (!isCollisionTankAndMap(0, 1, mapItems) && !isCollisionTankAndTank(0, 1, tanks)) {
                    y += SPEED;
                }
                break;
            default:
                break;
        }
    }

    public void moveEnemyTank(List<MapItem> mapItems, List<Tank> tanks) {
        random = new Random();
        int percent = random.nextInt(100);
        if (percent > 98) {
            orient = random.nextInt(4);
        }
        move(mapItems, tanks);
    }

    public void fire(List<Bullet> bulletTanks) {
        if (time + 400 > System.currentTimeMillis()) {
            return;
        }
        time = System.currentTimeMillis();
        int sizeBullet = Bullet.SIZE;
        int xBullet = x + SIZE / 2 - sizeBullet / 2;
        int yBullet = y + SIZE / 2 - sizeBullet / 2;
        Bullet bullet = new Bullet(xBullet, yBullet, orient);
        bulletTanks.add(bullet);
    }

    public void fireEnemyTank(List<Bullet> bulletEnemyTanks) {
        random = new Random();
        int temp = random.nextInt(100);
        if (temp > 98) {
            fire(bulletEnemyTanks);
        }
    }

    private boolean isCollisionTankAndMap(int i, int j, List<MapItem> mapItems) {
        for (int k = 0; k < mapItems.size(); k++) {
            if (isCollision(i, j, mapItems.get(k).getRect())) {
                return true;
            }
        }
        return false;
    }

    private boolean isCollisionTankAndTank(int i, int j, List<Tank> tanks) {
        for (int k = 0; k < tanks.size(); k++) {
            if (isCollision(i, j, tanks.get(k).getRect())) {
                return true;
            }
        }
        return false;
    }

    public boolean isCollideWithBullet(List<Bullet> bullets) {
        for (int k = 0; k < bullets.size(); k++) {
            if (isCollision(bullets.get(k).getRect())) {
                return true;
            }
        }
        return false;
    }

    public boolean isCollisionWithTank(List<Tank> tanks) {
        for (int k = 0; k < tanks.size(); k++) {
            if (isCollision(tanks.get(k).getRect())) {
                return true;
            }
        }
        return false;
    }

    public Rectangle2D getRect() {
        return new Rectangle(x, y, SIZE, SIZE);
    }

    public Rectangle2D getRect(int i, int j) {
        return new Rectangle(x + i, y + j, SIZE, SIZE);
    }

    public boolean isCollision(Rectangle2D rect) {
        Rectangle2D dest = new Rectangle();
        Rectangle2D.intersect(getRect(), rect, dest);
        if (dest.isEmpty()) {
            return false;
        }
        return true;
    }

    public boolean isCollision(int i, int j, Rectangle2D rect) {
        Rectangle2D dest = new Rectangle();
        Rectangle2D.intersect(getRect(i, j), rect, dest);
        if (dest.isEmpty()) {
            return false;
        }
        return true;
    }

    public void setOrient(int orient) {
        this.orient = orient;
    }
}

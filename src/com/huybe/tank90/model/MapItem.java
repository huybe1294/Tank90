package com.huybe.tank90.model;


import com.huybe.tank90.manager.ImageStore;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class MapItem {
    public static final int SIZE = 20;
    public static final int BOSS = 9;
    public static final int BRICK = 1;
    public static final int ROCK = 5;
    public static final int TREE = 4;
    public static final int WATER = 2;

    private int x;
    private int y;
    private int type;

    public MapItem(int x, int y, int type) {
        this.x = x;
        this.y = y;
        this.type = type;
    }

    public void draw(Graphics2D graphics2D) {
        switch (type) {
            case BOSS:
                graphics2D.drawImage(ImageStore.IMG_BOSS, x, y, 2 * SIZE, 2 * SIZE, null);
                break;

            case BRICK:
                graphics2D.drawImage(ImageStore.IMG_BRICK, x, y, SIZE, SIZE, null);
                break;

            case ROCK:
                graphics2D.drawImage(ImageStore.IMG_ROCK, x, y, SIZE, SIZE, null);
                break;

            case TREE:
                graphics2D.drawImage(ImageStore.IMG_TREE, x, y, SIZE, SIZE, null);
                break;

            case WATER:
                graphics2D.drawImage(ImageStore.IMG_WATER, x, y, SIZE, SIZE, null);
                break;

            default:
                break;
        }
    }

    public boolean isCollisionWithBullet(List<Bullet> bullets) {
        for (int i = 0; i < bullets.size(); i++) {
            if (isCollision(bullets.get(i).getRect())) {
                return true;
            }
        }
        return false;
    }

    public boolean isCollision(Rectangle2D rect) {
        Rectangle2D dest = new Rectangle();
        Rectangle2D.intersect(getRect(), rect, dest);
        if (dest.isEmpty()) {
            return false;
        }
        return true;
    }

    public Rectangle2D getRect() {
        if (type == BOSS) {
            return new Rectangle(x, y, SIZE * 2, SIZE * 2);
        }
        return new Rectangle(x, y, SIZE, SIZE);
    }

}
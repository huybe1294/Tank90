package com.huybe.tank90.model;

import com.huybe.tank90.manager.ImageStore;

import java.awt.*;

public class Heart {
    public static final int SIZE = 15;

    private int x;
    private int y;

    public Heart(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public void draw(Graphics2D graphics2D) {
        graphics2D.drawImage(ImageStore.IMG_HEART, x, y, SIZE, SIZE, null);
    }
}

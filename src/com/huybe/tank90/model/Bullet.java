package com.huybe.tank90.model;

import com.huybe.tank90.manager.ImageStore;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.List;

public class Bullet {
    private int x;
    private int y;
    private int orient;
    private boolean isLive;

    public static final int UP = 0;
    public static final int DOWN = 1;
    public static final int LEFT = 2;
    public static final int RIGHT = 3;
    public static final int SIZE = 8;
    private static final int SPEED = 4;

    public Bullet(int x, int y, int dir) {
        this.x = x;
        this.y = y;
        this.orient = dir;
        isLive = true;
    }

    public void draw(Graphics2D graphics2D) {
        graphics2D.drawImage(ImageStore.IMG_BULLET, x, y, SIZE, SIZE, null);
    }

    public void move() {
        switch (orient) {
            case LEFT:
                x -= SPEED;
                break;
            case RIGHT:
                x += SPEED;
                break;
            case UP:
                y -= SPEED;
                break;
            case DOWN:
                y += SPEED;
                break;
            default:
                break;
        }
    }

    public boolean isCollisionWithMap(List<MapItem> mapItems) {
        for (int i = 0; i < mapItems.size(); i++) {
            if (isCollision(mapItems.get(i).getRect())) {
                return true;
            }
        }
        return false;
    }


    public boolean isCollisionWithTank(List<Tank> tanks) {
        for (int i = 0; i < tanks.size(); i++) {
            if (isCollision(tanks.get(i).getRect())) {
                return true;
            }
        }
        return false;
    }

    public boolean isCollideWithBullet(List<Bullet> bullets) {
        for (int i = 0; i < bullets.size(); i++) {
            if (isCollision(bullets.get(i).getRect())) {
                return true;
            }
        }
        return false;
    }

    private boolean isCollision(Rectangle2D rect) {
        Rectangle2D dest = new Rectangle();
        Rectangle2D.intersect(getRect(), rect, dest);
        if (dest.isEmpty()) {
            return false;
        }
        return true;
    }

    public Rectangle2D getRect() {
        return new Rectangle(x, y, SIZE, SIZE);
    }

    public boolean isLive() {
        return isLive;
    }

    public void setLive(boolean live) {
        isLive = live;
    }
}
